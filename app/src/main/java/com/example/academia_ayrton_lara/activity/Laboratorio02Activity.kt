package com.example.academia_ayrton_lara.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import com.example.academia_ayrton_lara.R
import kotlinx.android.synthetic.main.activity_laboratorio02.*

class Laboratorio02Activity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_laboratorio02)

        btnSend.setOnClickListener{
            sendData()
        }

        chkVacunas4.setOnClickListener {

            if(!chkVacunas4.isChecked){
                chkVacunas.isChecked = false
                chkVacunas1.isChecked = false
                chkVacunas2.isChecked = false
                chkVacunas3.isChecked = false
            }else{
                chkVacunas.isChecked = true
                chkVacunas1.isChecked = true
                chkVacunas2.isChecked = true
                chkVacunas3.isChecked = true
            }


        }
    }

    fun sendData(){
        val names = edtNames.text.toString()
        val age = edtAge.text.toString()
        var mascota = ""


        if(names.isEmpty()){
            Toast.makeText(this,"Ingrese el Nombre",Toast.LENGTH_LONG).show()
            return
        }

        if(age.isEmpty()){
            Toast.makeText(this,"Ingrese la Edad",Toast.LENGTH_LONG).show()
            return
        }

        //if(!rbMale.isChecked && !rbMale.isChecked){ }




        if(rbDog.isChecked){
            mascota = "Perro"
        }else if (rbCat.isChecked){
            mascota = "Gato"
        }else if (rbRabbit.isChecked){
            mascota = "Conejo"
        }

        var vacunas = ""

        if(chkVacunas.isChecked){
            vacunas = "$vacunas Distemper"
        }

        if(chkVacunas1.isChecked){
            vacunas = "$vacunas Parvovirus"
        }

        if(chkVacunas2.isChecked){
            vacunas = "$vacunas Hepatitis"
        }

        if(chkVacunas3.isChecked){
            vacunas = "$vacunas Leptospirosis"
        }

        if(vacunas.equals("")){
            Toast.makeText(this,"Seleccione una vacuna",Toast.LENGTH_LONG).show()
            return
        }

        val bundle = Bundle().apply {
            putString("KEY_NAMES",names)
            putString("KEY_AGE",age)
            putString("KEY_MASCOTA",mascota)
            putString("KEY_VACUNAS",vacunas)
        }

        //5. Navigation
        val intent = Intent(this,ResumeMascotaActivity::class.java).apply {
            putExtras(bundle)
        }
        startActivity(intent)


    }
}