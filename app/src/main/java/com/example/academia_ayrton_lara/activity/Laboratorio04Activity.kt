package com.example.academia_ayrton_lara.activity

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.academia_ayrton_lara.R
import com.example.academia_ayrton_lara.databinding.ActivityLaboratorio04Binding
import kotlinx.android.synthetic.main.activity_laboratorio04.*
import java.util.ArrayList

class Laboratorio04Activity : AppCompatActivity() {


    private lateinit var binding : ActivityLaboratorio04Binding
    private lateinit var adapter : ContactoAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLaboratorio04Binding.inflate(layoutInflater)
        setContentView(binding.root)
        configureAdapter()
        events()
    }


    private fun configureAdapter(){
        adapter = ContactoAdapter(){ contacto ->
            val bundle = Bundle().apply {
                putSerializable("KEY_POKEMON",contacto)
            }

        }

        binding.recyclerContacto.adapter = adapter


    }

    private fun events() {

        val contactos = listOf(
            Contacto("Juan ledesma","Android developer","jledems12@gmail.com"),
            Contacto("Ayrton Lara","Marketing","ayrton@gmail.com"),
            Contacto("Sara Carrera","Soporte","sara1231@gmail.com"),
            Contacto("Juan German","Atencio al Cliente","juan12312@gmail.com"),
            Contacto("Norma Soto","Ventas","normasd12@gmail.com"),
            Contacto("Juana Vela","RR.HH","juanas12312@gmail.com"),
            Contacto("Graciela Olea","Senior ","gracelas12@gmail.com"),
            Contacto("Alberto Carrea","Android developer","alberto12@gmail.com"),
            Contacto("Juan ledesma","Android developer","jledems12@gmail.com"),
            Contacto("Ayrton Lara","Marketing","ayrton@gmail.com"),
            Contacto("Sara Carrera","Soporte","sara1231@gmail.com"),
            Contacto("Juan German","Atencio al Cliente","juan12312@gmail.com"),
            Contacto("Norma Soto","Ventas","normasd12@gmail.com"),
            Contacto("Juana Vela","RR.HH","juanas12312@gmail.com",),
            Contacto("Graciela Olea","Senior ","gracelas12@gmail.com"),
            Contacto("Alberto Carrea","Android developer","alberto12@gmail.com"),




        )
        adapter.updateList(contactos)
    }
}