package com.example.academia_ayrton_lara.activity

import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import com.example.academia_ayrton_lara.R


class Laboratorio01Activity : AppCompatActivity() {

    private lateinit var btnProcesar : Button
    private lateinit var edtEdad : EditText
    private lateinit var tvResultado : TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_laboratorio01)

        btnProcesar = findViewById(R.id.btnProcesar)
        edtEdad = findViewById(R.id.edtEdad)
        tvResultado = findViewById(R.id.tvResultado)
    }

    override fun onResume() {
        super.onResume()
        btnProcesar.setOnClickListener{
            calcularMayorEdad()
        }
    }

    private fun calcularMayorEdad(){

        if(edtEdad.text.isNotEmpty()){

        if(edtEdad.text.toString().toInt() > 17){
            tvResultado.setText("Usted es mayor de edad")
            tvResultado.setTextColor(Color.parseColor("#32b711"))
        }else{
            tvResultado.setText("Usted es menor de edad")
            tvResultado.setTextColor(Color.parseColor("#ff0000"))
        }
        }else{
            Toast.makeText(this, "Debe ingresar su edad", Toast.LENGTH_SHORT).show()
            tvResultado.setText("[Resultado]")
            tvResultado.setTextColor(Color.parseColor("#000000"))
        }

    }
}