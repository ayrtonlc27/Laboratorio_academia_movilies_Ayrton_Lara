package com.example.academia_ayrton_lara.activity


import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.academia_ayrton_lara.R
import com.example.academia_ayrton_lara.databinding.ItemContactoBinding


class ContactoAdapter constructor(
    var contactos:List<Contacto> = listOf(),
    val contactoClickedListener: (Contacto)->Unit)
    : RecyclerView.Adapter<ContactoAdapter.ViewHolder>() {

    inner class ViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView){

        private val binding : ItemContactoBinding = ItemContactoBinding.bind(itemView)

        fun bind(contacto: Contacto) = with(binding) {

            var primeraletra = contacto.name
            var letra = primeraletra[0].toString()

            tvletra.text = letra.trim()
            tvNameContacto.text = contacto.name
            tvNamePerfil.text = contacto.perfil
            tvNameCorreo.text = contacto.correo

            root.setOnClickListener {
                contactoClickedListener(contacto)
            }

        }

    }

    fun updateList(contactos:List<Contacto>){
        this.contactos = contactos
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view : View = LayoutInflater.from(parent.context).inflate(R.layout.item_contacto,parent,false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contacto = contactos[position]
        holder.bind(contacto)
    }

    override fun getItemCount(): Int {
        return contactos.size
    }
    }