package com.example.academia_ayrton_lara.activity

import java.io.Serializable

data class Contacto(
    var name:String,
    var perfil:String,
    var correo:String) : Serializable