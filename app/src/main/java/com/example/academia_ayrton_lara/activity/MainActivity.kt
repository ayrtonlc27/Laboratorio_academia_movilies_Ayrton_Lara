package com.example.academia_ayrton_lara.activity

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.academia_ayrton_lara.R


class MainActivity : AppCompatActivity() {

    private lateinit var btnLabo01 :Button
    private lateinit var btnLabo02 :Button
    private lateinit var btnLabo03 :Button
    private lateinit var btnLabo04 :Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

         btnLabo01 = findViewById(R.id.btnTarea01)
         btnLabo02 = findViewById(R.id.btnTarea02)
         btnLabo03 = findViewById(R.id.btnTarea03)
         btnLabo04 = findViewById(R.id.btnTarea04)

    }

    override fun onResume() {
        super.onResume()

        btnLabo01.setOnClickListener{
            val numbersIntent = Intent(this@MainActivity, Laboratorio01Activity::class.java)
            startActivity(numbersIntent)
        }

        btnLabo02.setOnClickListener{
            val numbersIntent = Intent(this@MainActivity, Laboratorio02Activity::class.java)
            startActivity(numbersIntent)
        }

        btnLabo03.setOnClickListener{
            val numbersIntent = Intent(this@MainActivity, Laboratorio03Activity::class.java)
            startActivity(numbersIntent)
        }

        btnLabo04.setOnClickListener{
            val numbersIntent = Intent(this@MainActivity, Laboratorio04Activity::class.java)
            startActivity(numbersIntent)
        }
    }


}