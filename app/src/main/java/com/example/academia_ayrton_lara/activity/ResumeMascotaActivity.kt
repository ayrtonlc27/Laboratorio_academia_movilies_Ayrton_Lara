package com.example.academia_ayrton_lara.activity

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.academia_ayrton_lara.R
import kotlinx.android.synthetic.main.activity_resume_mascota.*

class ResumeMascotaActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_resume_mascota)

        val bundle = intent.extras

        val mascota = bundle?.getString("KEY_MASCOTA") ?: "Desconocido"
        val names = bundle?.getString("KEY_NAMES") ?: "Desconocido"
        val age = bundle?.getString("KEY_AGE") ?: "Desconocido"
        val vacunas = bundle?.getString("KEY_VACUNAS") ?: "Desconocido"


        tvNombre.text = names
        tvEdad.text = age
        tvmascota.text = mascota
        tvvacuna.text = vacunas

        if(mascota.equals("Perro")){
            lottie.setAnimation(R.raw.dog)
            lottie.loop(true);
            lottie.playAnimation();
        }else if (mascota.equals("Gato")){
            lottie.setAnimation(R.raw.cat)
            lottie.loop(true)
            lottie.playAnimation();
        }else if (mascota.equals("Conejo")){
            lottie.setAnimation(R.raw.rabbit)
            lottie.loop(true)
            lottie.playAnimation();
        }


    }
}